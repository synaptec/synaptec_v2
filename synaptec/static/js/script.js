var addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
};

addEvent(window, "resize", function(event) {
    var h = window.innerHeight;
    var w = window.innerWidth;
    if (w > h & w < 768 & h < 440) { // horizontal mobile
        document.getElementById("div-xs").className = "links-xs-horizontal hidden-lg hidden-md hidden-sm col-xs-12"
        document.getElementById("foot1").className = "icons col-md-3 col-xs-4 col-xs-push-8"
        document.getElementById("foot2").className = "col-md-6 col-xs-4"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-xs-4 col-xs-pull-8"
    } else if (w <= 768 & h > 706) { // vertical mobile
        document.getElementById("div-xs").className = "links-sm hidden-lg hidden-md hidden-sm col-xs-12"
    } else {
        document.getElementById("div-xs").className = "links-xs hidden-lg hidden-md hidden-sm col-xs-12"
        document.getElementById("foot1").className = "icons col-md-3 col-sm-4 col-sm-push-8"
        document.getElementById("foot2").className = "col-md-6 col-sm-4 hidden-xs"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-sm-4 col-sm-pull-8"
    }
});

addEvent(window, "load", function(event) {
    var h = window.innerHeight;
    var w = window.innerWidth;
    if (w > h & w < 768 & h < 440) { // horizontal mobile
        document.getElementById("div-xs").className = "links-xs-horizontal hidden-lg hidden-md hidden-sm col-xs-12"
        document.getElementById("foot1").className = "icons col-md-3 col-xs-4 col-xs-push-8"
        document.getElementById("foot2").className = "col-md-6 col-xs-4"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-xs-4 col-xs-pull-8"
    } else if (w <= 768 & h > 706) { // vertical mobile
        document.getElementById("div-xs").className = "links-sm hidden-lg hidden-md hidden-sm col-xs-12"
    } else {
        document.getElementById("div-xs").className = "links-xs hidden-lg hidden-md hidden-sm col-xs-12"
        document.getElementById("foot1").className = "icons col-md-3 col-sm-4 col-sm-push-8"
        document.getElementById("foot2").className = "col-md-6 col-sm-4 hidden-xs"
        document.getElementById("foot3").className = "year montserrat col-md-3 col-sm-4 col-sm-pull-8"
    }
});

// window.location.reload(false); 